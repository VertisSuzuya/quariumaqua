#ifndef LEVELSCENE_H
#define LEVELSCENE_H

#include "colorUtils.h"
#include "scene.h"
#include "element.h"
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <vector>

struct Food{
    int timeToDeath = 2000;
    SDL_Rect rectangle;
    SDL_Texture* texture;
    SDL_Point position;
    Uint32 ticks;
    Uint32 sprite;
    SDL_Rect spriteRect;
};

struct FoodManager{
    std::vector<Food> foods;
    int currentFoodLevel = 1;
    std::string diffFood[3] {"bin/food1sprite.png","bin/food2.png","bin/food3.png"};
    int maxCount = 1;
    int countAlive = 0;
};


struct Item{
    SDL_Texture* texture;
    SDL_Rect rectangle;
    int numberOfFrames;
    int totalWidth;
};

class levelScene: public Scene {
public:

    levelScene(SDL_Renderer* renderer, Game& game) : Scene(renderer, game) {}
    
    void init() override;
    void update(float deltaTime) override;
    void render(float deltaTime) override;

private:
    void toDelete(int i);

    

    FoodManager foodManager;
    Food spawnFood(SDL_Point point);
    //the items in your inventory based on level
    Item items[8];
    Item backGround;
    //all the elements inside the scene
    Element colorLabel;
    Element musicLabel;
    Element fullscreenLabel;
    Element moneyLabel;
    Element timeLabel;

    Element backButton;
    Element settingsButton;

    Element UIWindow;
    Element splitButton9Window;
    Element settingsWindow1;
    Element settingsWindow2;
    Element foodSpawnPlaceWindow;
    Element groundWindow;

    Element colorLine1;
    Element colorLine2;
    Element musicLine1;
    Element musicLine2;

    Element fullscreenBox1;
    Element fullscreenBox2;

    Element UIButtons[10];

    //point used for mouse location
    SDL_Point point;

    // money system
    float timeInMiliseconds = 0;
    std::string timeStr = "00:00";
    std::string tmpstring;

    int seconds = 0;
    int minutes = 0;
    std::string tmpMinutes;
    std::string tmpSeconds;



    int money = 10000;
    std::string moneyStr = "$";

    //variabels for pressing buttons etc
    bool holdingC = false;
    bool holdingM = false;
    bool inSettings = false;
    bool mouseWasPressed = true;
    bool keyWasPressed = false;
    
    Uint32 ticks;
    Uint32 sprite;
    SDL_Rect srcrect;


    //functions to seperate code

    void spriteManagement();
    void initFoodMechanics();
    void printTime(float deltaTime);
    void printMoney();
    void settingsOffOn(uint8_t stateOfMouse, const uint8_t* keyBoardState);
    void setColorOfElements();
    void initRealItems();
    void renderSettings(SDL_Renderer* renderer);  
    void renderFood(SDL_Renderer* renderer);
    void initSettings();
    void initUI();
    void renderUI(SDL_Renderer* renderer);  
    TTF_Font* font = TTF_OpenFont("bin/atalon.otf", 35);

};

#endif