#ifndef SCENE_H
#define SCENE_H

#include <SDL2/SDL.h>
#include "game.h"


class Scene {
public: 
    SDL_Renderer* renderer;
    Game& game;
    
    Scene(SDL_Renderer* renderer, Game& game) : renderer(renderer), game(game) {}

    virtual ~Scene() {}
    virtual void init() = 0;
    virtual void update(float deltaTime) = 0;
    virtual void render(float delatTime) = 0;
protected:
    

};


#endif