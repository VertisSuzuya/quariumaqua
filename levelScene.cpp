#include "levelScene.h"
#include "menuScene.h"
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include "colorUtils.h"
#include <SDL2/SDL_mixer.h> 
#include <string>
#include <cstring>
#include <SDL2/SDL.h>

//#include "game.h"

#define BLACK (rgb{0, 0, 0})
#define WHITE (rgb{1, 1, 1})
#define GRAY (rgb{0.1,0.1,0.1})

#define COUNTOFUIBUTTONS 10
#define COUNTOFINVENTORY 8

#define LINE_LEFT 560.0f
#define LINE_WIDTH 770.0f






void playMusic(Game& game){
    Mix_Music *gMusic = NULL;
        
    
    //Load music
    gMusic = Mix_LoadMUS( "bin/chillLevel.wav" );
    if( gMusic == NULL )
    {
        printf( "Failed to load beat music! SDL_mixer Error: %s\n", Mix_GetError() );
    }

    Mix_PlayMusic( gMusic, -1 );
    printf("start lvl volume %f\n", game.musicVolume);
    Mix_VolumeMusic(game.musicVolume);

}




void levelScene::initRealItems(){
    int w = 0, h = 0;
        for(size_t i = 0; i < COUNTOFINVENTORY; i++){
            if(strcmp(game.levels[game.currentLevel].whatItems[i].c_str(), "") == 0){
                //printf("%s %d\n", game.levels[game.currentLevel].whatItems[i].c_str(), game.currentLevel);
                items[i].texture =  IMG_LoadTexture(renderer, "bin/closedDoor.png");
                
                if(items[i].texture == NULL){
                    printf("chyba\n");
                }
            } else {
                //printf("%s %d\n", game.levels[game.currentLevel].whatItems[i].c_str(), game.currentLevel);
                items[i].texture =  IMG_LoadTexture(renderer, (game.levels[game.currentLevel].whatItems[i].c_str()));
                if(items[i].texture == NULL){
                    printf("chyba\n");
                }
            }
            SDL_QueryTexture(items[i].texture, nullptr, nullptr, &w, &h);
            items[i].rectangle.x = (1920/10 * i) + 10;
            items[i].rectangle.y = 0 + 10;
            items[i].rectangle.w = 192 - 20;
            items[i].rectangle.h = 150 - 20;
        }

        backGround.texture = IMG_LoadTexture(renderer, (game.levels[game.currentLevel].whatBackground.c_str()));
        SDL_QueryTexture(backGround.texture, nullptr, nullptr, &w, &h);
        backGround.rectangle.x = 0;
        backGround.rectangle.y = 0;
        backGround.rectangle.w = game.widthOfWindow;
        backGround.rectangle.h = game.heightOfWindow;



}


void levelScene::initSettings(){

    colorLabel = Element("UI Color settings", font, renderer, false, true, 0, 0, 830, 180, game);
    fullscreenLabel = Element("Toggle fullscreen", font, renderer, false, true, 0, 0, 820, 380, game);
    musicLabel = Element("Music bar", font, renderer, false, true, 0, 0, 880, 580, game);

    backButton = Element("Exit to main menu", font, renderer, true, true, 0, 0, 800, 800, game);
    settingsButton = Element("Settings", font, renderer, true, true, 0, 0, (1920/10 * 9) + 30, 0 + 10 + 50, game);

    settingsWindow1 = Element(true, false, 870, 1000, 1920 / 2 - 1000 / 2, 100, game);
    settingsWindow1.transparent = 220;
    settingsWindow2 = Element(true, false, 830, 960, 1920 / 2 - 960 / 2, 120, game);
    settingsWindow2.transparent = 240;

    colorLine1 = Element(false, false, 10, LINE_WIDTH, LINE_LEFT, 300, game);
    colorLine1.transparent = 150;
    colorLine2 = Element(false, true, 40, 10, LINE_LEFT, 285, game);
    colorLine2.transparent = 250;
    musicLine1 = Element(false, false, 10, LINE_WIDTH, LINE_LEFT, 700, game);
    musicLine1.transparent = 150;
    musicLine2 = Element(false, false, 40, 10, LINE_LEFT, 685, game);
    musicLine2.transparent = 250;

    fullscreenBox1 = Element(false, false, 40, 40, 940, 480, game);
    fullscreenBox2 = Element(false, true, 30, 30, 945, 485, game);

}

void levelScene::initUI(){

    UIWindow = Element(true, false, 150, 1920, 0, 0, game);
    splitButton9Window = Element(true, false, 20, 192 - 20, (1920/10 * 8) + 10, 0 + 10 + 55, game);

    for(size_t i = 0; i < COUNTOFUIBUTTONS; i++){
        UIButtons[i] = Element(false, true, 150 - 20, 192 - 20, (1920/10 * i) + 10, 0 + 10, game);
        UIButtons[i].transparent = 210;
    }

    moneyLabel = Element(moneyStr, font, renderer, false, true, 0, 0, (1920/10 * 8) + 30,  0 + 10 + 10, game);

    timeLabel = Element(timeStr, font, renderer, false, true, 0, 0, (1920/10 * 8) + 30 + 20,  0 + 10 + 10 + 80, game);
}

void levelScene::initFoodMechanics(){
    foodSpawnPlaceWindow = Element(false, true, 830, 1920, 0, 150, game);
    foodSpawnPlaceWindow.transparent = 100;

    groundWindow = Element(false, true, 100, 1920, 0, 980, game);
    groundWindow.transparent = 200;
}

void levelScene::init() {
    SDL_GetWindowSize(game.window, &game.widthOfWindow, &game.heightOfWindow);

    playMusic(game);
    initUI();
    initRealItems();

    initSettings();
        
    printf("init levelu done\n");

    initFoodMechanics();
    /*test=SDL_Point point1; point1.x = 1; point1.y = 6;
    SDL_Point point2; point2.x = 7; point2.y = 2;
    SDL_Point point3; point3 = point1 + point2;
    printf("%d, %d\n", point3.x, point3.y);*/

    
}

void levelScene::setColorOfElements(){
    colorLabel.setColor(game.UIcolor);
    fullscreenLabel.setColor(game.UIcolor);
    backButton.setColor(game.UIcolor);
    musicLabel.setColor(game.UIcolor);
    settingsButton.setColor(game.UIcolor);
    moneyLabel.setColor(game.UIcolor);
    timeLabel.setColor(game.UIcolor);
}

void levelScene::settingsOffOn(uint8_t stateOfMouse, const uint8_t* keyBoardState){
    if((stateOfMouse & SDL_BUTTON_LEFT) && !mouseWasPressed && SDL_PointInRect(&point, &UIButtons[9].rectangle)){
        if(inSettings){
            //printf("vypnuto\n");

            inSettings = false;
            
        } else {
            //printf("zapnuto\n");
            inSettings = true;
        }
    }

    if(keyBoardState[SDL_SCANCODE_ESCAPE] && !keyWasPressed){

        if(inSettings){
            //printf("vypnuto\n");

            inSettings = false;
            
        } else {
            //printf("zapnuto\n");
            inSettings = true;
        }
    }
}

void levelScene::printMoney(){
//money += 1;
        if(money < 10){
            tmpstring = "$00000";
        } else if (money < 100){
            tmpstring = "$0000";
        } else if (money < 1000){
            tmpstring = "$000";
        } else if (money < 10000){
            tmpstring = "$00";
        } else if (money < 100000){
            tmpstring = "$0";
        } else {
            tmpstring = "$";
        }
        moneyStr = tmpstring + std::to_string(money);
               
        //printf("%s\n", moneyStr.c_str());
        moneyLabel = Element(moneyStr, font, renderer, false, true, 0, 0, (1920/10 * 8) + 30,  0 + 10 + 10, game);
}

void levelScene::printTime(float deltaTime){
    timeInMiliseconds += deltaTime;
        
    if(((timeInMiliseconds / 1000) > 59)){
        minutes = (timeInMiliseconds / 1000) / 60.0f;
        seconds = (int)(timeInMiliseconds / 1000) % 60;
    } else {
        minutes = 0;
        seconds = (int)(timeInMiliseconds / 1000) % 60;
    }
    if(seconds < 10){
        tmpSeconds = "0";
    } else {
        tmpSeconds = "";
    }

    if(minutes < 10){
        tmpMinutes = "0";
    } else {
        tmpMinutes = "";
    }
    timeStr = tmpMinutes + std::to_string(minutes) + ":" + tmpSeconds +std::to_string(seconds);
    timeLabel = Element(timeStr, font, renderer, false, true, 0, 0, (1920/10 * 8) + 30 + 20,  0 + 10 + 10 + 80, game);
}



Food levelScene::spawnFood(SDL_Point point){
    Food tmpFood;
    tmpFood.position.x = point.x;
    tmpFood.position.y = point.y;
    tmpFood.rectangle.h = 64;
    tmpFood.rectangle.w = 64;
    tmpFood.rectangle.x = point.x - 32;
    tmpFood.rectangle.y = point.y - 32;
    
    tmpFood.texture = IMG_LoadTexture(renderer, foodManager.diffFood[foodManager.currentFoodLevel - 1].c_str());

    return tmpFood;

}

void levelScene::spriteManagement(){
    ticks = SDL_GetTicks();

    sprite = (ticks / 100) % 8;
    srcrect.h = 64;
    srcrect.w = 64;
    srcrect.x = sprite * 64;
    srcrect.y = 0;

}

void levelScene::toDelete(int i){
  
        foodManager.foods[i].ticks = SDL_GetTicks() + i * 69420;
        foodManager.foods[i].sprite = (ticks / 100) % 8;
        foodManager.foods[i].spriteRect.h = 64;
        foodManager.foods[i].spriteRect.w = 64;
        foodManager.foods[i].spriteRect.x = sprite * 64;
        foodManager.foods[i].spriteRect.y = 0;

    
}

void levelScene::update(float deltaTime){


    spriteManagement();

    const uint8_t* keyBoardState = SDL_GetKeyboardState(nullptr);
    
    uint8_t stateOfMouse = SDL_GetMouseState(&point.x, &point.y);
    
    settingsOffOn(stateOfMouse, keyBoardState);

    //moc cringe, ale funguje xd
    float angle;

    hsv hsvColor;

    //if the player is in settings, the game is paused - this code doesnt really need any changing, since its mostly done
    //I would like to maybe change the struct of the code to make it pretier, but it works just fine like this 
    if(inSettings){


        if((stateOfMouse & SDL_BUTTON_LEFT) && !mouseWasPressed && SDL_PointInRect(&point, &backButton.rectangle)){
            game.setCurrentScene(new menuScene(renderer, game));
        }
        if((stateOfMouse & SDL_BUTTON_LEFT) && SDL_PointInRect(&point, &fullscreenBox1.rectangle) && !mouseWasPressed){
            if(game.fullscreen){
                game.fullscreen = false;
                SDL_SetWindowFullscreen(game.window, SDL_WINDOW_BORDERLESS);
            } else {
                game.fullscreen = true;
                SDL_SetWindowFullscreen(game.window, SDL_WINDOW_FULLSCREEN);

            }
            
        }

        if((stateOfMouse & SDL_BUTTON_LEFT) && SDL_PointInRect(&point, &musicLine1.rectangle)){
            musicLine2.rectangle.x = point.x;
        }

        if((stateOfMouse & SDL_BUTTON_LEFT) && SDL_PointInRect(&point, &colorLine1.rectangle)){
            colorLine2.rectangle.x = point.x;
        }
    
        if(holdingM == true){

            game.musicVolume = ((musicLine2.rectangle.x - LINE_LEFT) / (LINE_WIDTH + LINE_LEFT - LINE_LEFT)) * MIX_MAX_VOLUME;
            //game.musicVolume = theVolume;
            Mix_VolumeMusic(game.musicVolume);
            printf("lvl volume: %f\n", game.musicVolume);


            if(point.x < (LINE_WIDTH + LINE_LEFT - 10)){

                musicLine2.rectangle.x = point.x;
            } else {
                musicLine2.rectangle.x = LINE_WIDTH + LINE_LEFT - 10;
            }

            if(point.x > LINE_LEFT){

            } else {
                musicLine2.rectangle.x = LINE_LEFT;
            }
            if(!(stateOfMouse & SDL_BUTTON_LEFT))
            holdingM = false;

        } else {
            if((stateOfMouse & SDL_BUTTON_LEFT) && SDL_PointInRect(&point, &musicLine2.rectangle)){
                holdingM = true;
            }
        }

        if((stateOfMouse & SDL_BUTTON_LEFT) && SDL_PointInRect(&point, &musicLine2.rectangle)){
            musicLine2.rectangle.x = point.x;
        }


        if(holdingC == true){
            angle = ((colorLine2.rectangle.x - LINE_LEFT) / (LINE_WIDTH + LINE_LEFT - LINE_LEFT)) * 360 - 1;
            printf("lvl angle: %f\n", angle);
            hsvColor.s = 1;
            hsvColor.v = 1;
            hsvColor.h = angle;
            game.UIcolor = hsv2rgb(hsvColor);

            

            printf("%f, %f, %f\n", game.UIcolor.r, game.UIcolor.g, game.UIcolor.b);
            
            



            if(point.x < (LINE_WIDTH + LINE_LEFT - 10)){

                colorLine2.rectangle.x = point.x;
                
            } else {
                colorLine2.rectangle.x = LINE_WIDTH + LINE_LEFT - 10;
            }

            if(point.x > LINE_LEFT){

            } else {
                colorLine2.rectangle.x = LINE_LEFT;
            }
            if(!(stateOfMouse & SDL_BUTTON_LEFT))
            holdingC = false;
        } else {
            if((stateOfMouse & SDL_BUTTON_LEFT) && SDL_PointInRect(&point, &colorLine2.rectangle)){
                holdingC = true;
            }
        }
        
        
        if((stateOfMouse & SDL_BUTTON_LEFT) && SDL_PointInRect(&point, &colorLine2.rectangle)){
            colorLine2.rectangle.x = point.x;
          
        }

    
    
    } else {
    //here is where the game mechanincs have to go to, since the game is unpaused. 
    //First i need to check where the player presses and then acording to that have a mechanic to continue 
    //if he presses randomly into the water and he doesnt already have the limit of food items he can have at one time
    //its going to spawn a food on the cursor where he clicked, then its slowly going to move downard until it hits the floor
    //after hitting the floor its going to diseaper in 2 seconds  
        if((stateOfMouse & SDL_BUTTON_LEFT) && !mouseWasPressed && SDL_PointInRect(&point, &UIButtons[2].rectangle)){
            int moneyNeeded = INT_MAX;
            if(foodManager.maxCount == 1){
                moneyNeeded = 200;
            }
            if(foodManager.maxCount == 2){
                moneyNeeded = 300;
            }
            /*if(foodManager.maxCount == 3){
                moneyNeeded = 400;
            }*/
            if(money >= moneyNeeded){
                money -= moneyNeeded;
                foodManager.maxCount ++;
            }
        }

        if((stateOfMouse & SDL_BUTTON_LEFT) && !mouseWasPressed && SDL_PointInRect(&point, &UIButtons[1].rectangle)){
            int moneyNeeded = INT_MAX;
            if(foodManager.currentFoodLevel == 1){
                moneyNeeded = 300;
            }
            if(foodManager.currentFoodLevel == 2){
                moneyNeeded = 400;
            }
            if(money >= moneyNeeded){
                money -= moneyNeeded;
                foodManager.currentFoodLevel ++;
            }
        }



        for(size_t i = 0; i < foodManager.foods.size(); i++){
            if(foodManager.foods[i].timeToDeath <= 0){
                foodManager.foods.erase(foodManager.foods.begin() + i);
                foodManager.countAlive -= 1;
            } else {
                
                if(SDL_PointInRect(&foodManager.foods[i].position, &groundWindow.rectangle)){
                    foodManager.foods[i].timeToDeath -= deltaTime;
                } else { 
                    toDelete(i);
                    foodManager.foods[i].position.y += 1;

                    
                    foodManager.foods[i].rectangle.y += 1;


                }
            }
        }


        //if he hits a coin or enemy do something, if nothing is hit do else
        if(0){

        } else {

            if(money >= 10 && stateOfMouse & SDL_BUTTON_LEFT && SDL_PointInRect(&point, &foodSpawnPlaceWindow.rectangle) && (foodManager.countAlive < foodManager.maxCount) && !mouseWasPressed){
                foodManager.foods.push_back(spawnFood(point));
                money -= 10;
                foodManager.countAlive ++;

            }
        }

        printMoney();
        printTime(deltaTime);
    }

    setColorOfElements();

    keyWasPressed = keyBoardState[SDL_SCANCODE_ESCAPE];
    mouseWasPressed = stateOfMouse & SDL_BUTTON_LEFT;

}

void levelScene::renderUI(SDL_Renderer* renderer){


    UIWindow.render(renderer, game.UIcolor);

    for(size_t i = 0; i < COUNTOFUIBUTTONS; i++){
        UIButtons[i].render(renderer, BLACK);
    }
   
    splitButton9Window.render(renderer, game.UIcolor);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255); 

    moneyLabel.render(renderer);
    timeLabel.render(renderer);
    settingsButton.render(renderer);
 

    for(size_t i = 0; i < COUNTOFINVENTORY; i++){
        if(i == 2){
            if(foodManager.maxCount == 2){
                items[i].texture =  IMG_LoadTexture(renderer, "bin/2x.png");

            }
            if(foodManager.maxCount == 3){
                items[i].texture =  IMG_LoadTexture(renderer, "bin/3x.png");
            }
        }

        if(i == 1){
            if(foodManager.currentFoodLevel == 2){
                items[i].texture =  IMG_LoadTexture(renderer, "bin/food2.png");

            }
            if(foodManager.currentFoodLevel == 3){
                items[i].texture =  IMG_LoadTexture(renderer, "bin/food3.png");
            }
        }

        SDL_RenderCopyEx(renderer, items[i].texture, &srcrect, &items[i].rectangle, 0, nullptr, SDL_FLIP_NONE);
    }

}

void levelScene::renderSettings(SDL_Renderer* renderer){

    settingsWindow1.render(renderer, game.UIcolor);
    settingsWindow2.render(renderer, BLACK);

    colorLine1.renderOuter(renderer, WHITE);
    colorLine2.render(renderer, WHITE);
    musicLine1.renderOuter(renderer, WHITE);
    musicLine2.render(renderer, WHITE);

    fullscreenBox1.render(renderer, game.UIcolor);
    if( game.fullscreen == true)
        fullscreenBox2.render(renderer, BLACK);

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

    colorLabel.render(renderer);
    musicLabel.render(renderer);
    backButton.render(renderer);
    fullscreenLabel.render(renderer);

}

void levelScene::renderFood(SDL_Renderer* renderer){
    foodSpawnPlaceWindow.render(renderer, BLACK);
    groundWindow.render(renderer, BLACK);

    for(size_t i = 0; i < foodManager.foods.size(); i++){
        SDL_RenderCopy(renderer, foodManager.foods[i].texture, &foodManager.foods[i].spriteRect, &foodManager.foods[i].rectangle);

    }

}

void levelScene::render(float deltaTime){
    SDL_RenderClear(renderer);

    SDL_RenderCopy(renderer, backGround.texture, nullptr, &backGround.rectangle);

    renderUI(renderer);

    renderFood(renderer);


    if(inSettings)
        renderSettings(renderer);



    SDL_RenderPresent(renderer);
}
