#ifndef MENUSCENE_H
#define MENUSCENE_H

#include "element.h"
#include "colorUtils.h"
#include "scene.h"
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>



class menuScene: public Scene {


public:
    bool inSettings = false;
    bool wasPressed = false;

    menuScene(SDL_Renderer* renderer, Game& game) : Scene(renderer, game) {}

    void init() override;
    void update(float deltaTime) override;
    void render(float deltaTime) override;

private:

    
    Element titleLabel;
    Element colorLabel;
    Element fullscreenLabel; 
    Element musicLabel;

    Element startButton;
    Element settingsButton;
    Element quitButton;

    Element settingsWindow1;
    Element settingsWindow2;
    Element rightWindow1;
    Element rightWindow2;

    Element colorLine1;
    Element colorLine2;
    Element musicLine1;
    Element musicLine2;

    Element fullscreenBox1;
    Element fullscreenBox2;

    bool mouseWasPressed = false;

    void createStaticText();
    void drawSettings(SDL_Renderer* renderer);
    void drawTheRest(SDL_Renderer* renderer);
    TTF_Font* font = TTF_OpenFont("bin/atalon.otf", 60);

    bool holdingC = false;
    bool holdingM = false;
    SDL_Point point;
    SDL_Point pointM;
 
};


#endif