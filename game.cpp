#include "game.h"
#define SDL_MAIN_HANDLED

#include <filesystem>
#include <iostream>

#include "scene.h"
#include <SDL2/SDL.h>
#include <stdio.h>
#include <chrono>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>

void Game::fillLevelInfo(int i){
    switch(i){
        case 0:
            levels[0].chanceToSpawnEnemy = 2;
            levels[0].whatEnemies[0] = 1;

            levels[0].priceOfChest = 200;

            levels[0].whatItems[0] = "bin/fish1sprite.png";
            levels[0].whatItems[1] = "bin/food1sprite.png";
            levels[0].whatItems[2] = "bin/1x.png";
            levels[0].whatItems[7] = "bin/chest1.png";
            
            levels[0].whatBackground = "bin/background1.png";

            

        break;

        case 1:
        break;

        case 2:
        break;

        default:
        break;
    }
}

void Game::init(){
    for(size_t i = 0; i < level_count; i++){
        fillLevelInfo(i);
    }
}

void Game::setCurrentScene(Scene* scene){
    
    nextScene = scene;
    
}

void Game::runGame() {
    
    printf( "runGame\n");
    using namespace std::chrono;
    SDL_Event event;
    
    steady_clock::time_point lastTime = steady_clock::now();
    currentScene = nextScene;
    currentScene->init();
    nextScene = nullptr;
    while( quit == false ){
            steady_clock::time_point currentTime = steady_clock::now();
            float deltaTime = duration_cast<microseconds>(currentTime - lastTime).count() / 1000.0f;
            while( SDL_PollEvent( &event ) ) { 
                if( event.type == SDL_QUIT ) 
                    quit = true; 
            }

            currentScene->update(deltaTime);
            currentScene->render(deltaTime);
            if(nextScene != nullptr){
                delete currentScene;
                currentScene = nextScene;
                nextScene->init();
                nextScene = nullptr;
            }

            lastTime = currentTime;
        }

}

