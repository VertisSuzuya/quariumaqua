#include "menuScene.h"
#include "levelScene.h"
#include "scene.h"
#include "game.h"

#define SDL_MAIN_HANDLED

#include <filesystem>
#include <iostream>

#include <SDL2/SDL.h>
#include <stdio.h>
#include <chrono>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>


const int SCREEN_WIDTH = 1920;
const int SCREEN_HEIGHT = 1080;


int main (int argc, char* args[]){
    //The window we'll be rendering to
    using namespace std::chrono;

    SDL_Window* window = NULL;
    
    //The surface contained by the window
    //SDL_Surface* screenSurface = NULL;

    //Initialize SDL
    if( SDL_Init( SDL_INIT_VIDEO |  SDL_INIT_AUDIO) < 0 )
    {
        printf( "SDL could not initialize! SDL_Error: %s\n", SDL_GetError() );\
        return 0;
    } 

    //Initialize SDL_mixer
                if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0 )
                {
                    printf( "SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError() );
                    
                }

    int imgFlags = IMG_INIT_PNG;
    if( !( IMG_Init( imgFlags ) & imgFlags ) ) {
        printf( "asdfasdfasdfasdfasdfa\n");
    }

    if( TTF_Init() == -1 ) {
        printf("chyba ttf init\n");
    }


    SDL_Renderer* renderer;

    //Create window
    window = SDL_CreateWindow( "QuariumAqua", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
    SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN);

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

   
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
    if( window == NULL )
    {
        printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
    } else {
        //Get window surface
        //screenSurface = SDL_GetWindowSurface( window );

        //Fill the surface black        
        //Update the surface
        SDL_UpdateWindowSurface( window );

        
        Game game(window);
               
        game.UIcolor.r = 1;
        game.UIcolor.g = 1;
        game.UIcolor.b = 1;
        game.musicVolume = 15;
        game.currentLevel = 0; //načíst ze souboru.

        game.init();
        printf("about to create menuscene\n");
        game.setCurrentScene(new menuScene(renderer, game));

        game.runGame();

    }

        //Destroy window
    SDL_DestroyWindow( window );

    //Quit SDL subsystems
    SDL_Quit();

    return 0;
}
