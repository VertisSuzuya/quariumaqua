#include "element.h"
//#include "game.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "colorUtils.h"
#include <stdio.h>
#include <string>
#include <cstring>

Element::Element(std::string text, TTF_Font* font, SDL_Renderer* renderer, bool pressable, bool changingColor, int height, int width, int x, int y, Game& game){
        //printf("in\n");
        printf("%s\n", text.c_str());
        if(font == nullptr){
            printf("random\n");
        }
    SDL_Surface* surface = TTF_RenderText_Solid(font, text.c_str(), SDL_Color{255,255,255,255});
                //printf("in\n");

    texture = SDL_CreateTextureFromSurface(renderer, surface);
            

    float calculatedHeight = (height / 1080.0f) * game.heightOfWindow;
    float calculatedWidth = (width / 1920.0f) * game.widthOfWindow;
    float calculatedX = (x / 1920.0f) * game.widthOfWindow;
    float calculatedY = (y / 1080.0f) * game.heightOfWindow;
      

    rectangle.h = surface->h + calculatedHeight;
    rectangle.w = surface->w + calculatedWidth;
    rectangle.x = calculatedX;
    rectangle.y = calculatedY;
    SDL_FreeSurface(surface);
           

}

Element::Element(bool changingColor, bool pressable, int height, int width, int x, int y, Game& game){
    float calculatedHeight = (height / 1080.0f) * game.heightOfWindow;
    float calculatedWidth = (width / 1920.0f) * game.widthOfWindow;
    float calculatedX = (x / 1920.0f) * game.widthOfWindow;
    float calculatedY = (y / 1080.0f) * game.heightOfWindow;

    rectangle.h = calculatedHeight;
    rectangle.w = calculatedWidth;
    rectangle.x = calculatedX;
    rectangle.y = calculatedY; 
}


Element::Element(std::string text, bool pressable, bool changingColor, int height, int width, int x, int y, Game& game){
    
}


void Element::setColor(rgb UIColor){
    SDL_SetTextureColorMod(texture, UIColor.r * 255, UIColor.g * 255, UIColor.b * 255);

}

void Element::update(Scene* scene){
    if(onClick == nullptr){
        return;
    }

    SDL_Point point;
    uint8_t stateOfMouse = SDL_GetMouseState(&point.x, &point.y);
    const uint8_t* keyBoardState = SDL_GetKeyboardState(nullptr);
    if(whatKey == -1){
        
        if((SDL_PointInRect(&point, &rectangle) && (stateOfMouse & SDL_BUTTON_LEFT)) && !mouseWasPressed){   
            onClick(scene);
        }

    } else {
        

        if((keyBoardState[whatKey] != 0 && !wasPressed) || (SDL_PointInRect(&point, &rectangle) && (stateOfMouse & SDL_BUTTON_LEFT) && !mouseWasPressed)){
            onClick(scene);
        }
    }
    wasPressed = keyBoardState[whatKey];
    mouseWasPressed = stateOfMouse & SDL_BUTTON_LEFT;


}

void Element::render(SDL_Renderer* renderer){
    SDL_RenderCopy(renderer, texture, nullptr, &rectangle);

}

void Element::render(SDL_Renderer* renderer, rgb Color){
    SDL_SetRenderDrawColor(renderer,  Color.r * 255, Color.g * 255, Color.b * 255, transparent);
    SDL_RenderFillRect(renderer, &rectangle);
}

void Element::renderOuter(SDL_Renderer* renderer, rgb Color){
    SDL_SetRenderDrawColor(renderer,  Color.r * 255, Color.g * 255, Color.b * 255, transparent);
    SDL_RenderDrawRect(renderer, &rectangle);
}

