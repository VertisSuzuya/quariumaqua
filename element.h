#ifndef ELEMENT_H
#define ELEMENT_H

#include "game.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "colorUtils.h"
#include <stdio.h>
#include <string>
#include <cstring>

class Element {
public:
    int transparent = 255;

    SDL_Rect rectangle;
    int whatKey = -1;
    void (*onClick)(Scene* scene) = nullptr;
    Element(std::string text, TTF_Font* font, SDL_Renderer* renderer, bool pressable, bool changingColor, int height, int width, int x, int y, Game& game);
    Element(std::string text, bool pressable, bool changingColor, int height, int width, int x, int y, Game& game);
    Element(bool chaningColor, bool pressable, int height, int width, int x, int y, Game& game);
    Element(){};
    void setColor(rgb UIcolor);
    void update(Scene* scene);
    void render(SDL_Renderer* renderer);
    void render(SDL_Renderer* renderer, rgb Color);
    void renderOuter(SDL_Renderer* renderer, rgb Color);

private:
    SDL_Texture* texture;
    bool changingColor = true;
    bool pressable = true;
    bool wasPressed = false;
    bool mouseWasPressed = false;
};

#endif