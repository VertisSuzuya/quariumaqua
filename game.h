#ifndef GAME_H
#define GAME_H
#include <SDL2/SDL.h>
#include "colorUtils.h"
#include <string>
#include <stdio.h>


struct LevelData {
    int priceOfChest;
    int chanceToSpawnEnemy;
    int whatEnemies[5] = {0,0,0,0,0};
    std::string whatItems[8] = {"","","","","","","",""};
    int whatHelper[15] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    std::string whatBackground;
};

class Scene;
//int SCREEN_WIDTH, SCREEN_HEIGHT;
class Game {
    public:

    int currentLevel;
    static constexpr size_t level_count = 16;
    LevelData levels[level_count];

    SDL_Window* window;

    float musicVolume;
    bool fullscreen;
    rgb UIcolor;
    
    int widthOfWindow;
    int heightOfWindow;

    bool quit = false;

    void fillLevelInfo(int i);

    void init();
    
    void setCurrentScene(Scene* nextScene);

    void runGame();

    Game(SDL_Window* window) : window(window) {}

private:
Scene* currentScene = nullptr;
Scene* nextScene = nullptr;



};
#endif