#include "menuScene.h"
#include "levelScene.h"
#include "element.h"
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include "colorUtils.h"
#include <SDL2/SDL_mixer.h> 


#define BLACK (rgb{0, 0, 0})
#define WHITE (rgb{1, 1, 1})
#define GRAY (rgb{0.1,0.1,0.1})

#define LINE_LEFT 200.0f
#define LINE_WIDTH 770.0f

void changeToGameScene(Scene* scene){
    scene->game.setCurrentScene(new levelScene(scene->renderer, scene->game));
}

void quitGame(Scene* scene){
    scene->game.quit = true;
}

void settingsState(Scene* scene){
    
    if(((menuScene*)scene)->inSettings){
            //printf("vypnuto\n");

        ((menuScene*)scene)->inSettings = false;
            
    } else {
            //printf("zapnuto\n");
        ((menuScene*)scene)->inSettings = true;
    }
}

void changeScreenMode(Scene* scene){
    
}

void menuScene::init() {
    Mix_Music *gMusic = NULL;
        
    
    //Load music
    gMusic = Mix_LoadMUS( "bin/8bitWater.wav" );
    if( gMusic == NULL )
    {
        printf( "Failed to load beat music! SDL_mixer Error: %s\n", Mix_GetError() );
    }

    Mix_PlayMusic( gMusic, -1 );
    
    Mix_VolumeMusic(game.musicVolume);
    
    SDL_GetWindowSize(game.window, &game.widthOfWindow, &game.heightOfWindow);

    printf("init of music done\n");

    titleLabel = Element("QuariumAqua", font, renderer, false, true, 20, 80, 1370, 180, game);

    colorLabel = Element("UI Color settings", font, renderer, false, true, 0, 0, 350, 180, game);

    fullscreenLabel = Element("Toggle fullscreen", font, renderer, false, true, 0, 0, 350, 380, game);

    musicLabel = Element("Music bar", font, renderer, false, true, 0, 0, 450, 580, game);

    settingsButton = Element("Settings", font, renderer, true, true, 0, 0, 1370, 580, game);
    settingsButton.onClick = settingsState;
    settingsButton.whatKey = SDL_SCANCODE_S;

    startButton = Element("Start the Game", font, renderer, true, true, 0, 0, 1370, 380, game);
    startButton.onClick = changeToGameScene;

    quitButton = Element("Quit to desktop", font, renderer, true, true, 0, 0, 1370, 780, game);
    quitButton.onClick = quitGame;
    quitButton.whatKey = SDL_SCANCODE_ESCAPE;

    printf("init of elements done\n");


    settingsWindow1 = Element(true, false, 870, 1000, 100, 100, game);
    settingsWindow1.transparent = 220;
    settingsWindow2 = Element(true, false, 830, 960, 120, 120, game);
    settingsWindow2.transparent = 255;

    rightWindow1 = Element(true, false, 910, 1000, 1300, 80, game);
    rightWindow1.transparent = 220;
    rightWindow2 = Element(true, false, 870, 960, 1320, 100, game);
    rightWindow2.transparent = 255;

    colorLine1 = Element(false, false, 10, LINE_WIDTH, LINE_LEFT, 300, game);
    colorLine1.transparent = 150;
    colorLine2 = Element(false, true, 40, 10, LINE_LEFT, 285, game);
    colorLine2.transparent = 250;

    musicLine1 = Element(false, false, 10, LINE_WIDTH, LINE_LEFT, 700, game);
    musicLine1.transparent = 150;
    musicLine2 = Element(false, false, 40, 10, LINE_LEFT, 685, game);
    musicLine2.transparent = 250;

    fullscreenBox1 = Element(false, false, 40, 40, 580, 480, game);
    fullscreenBox2 = Element(false, true, 30, 30, 585, 485, game);
    

    //odklikavaci box v nastaveni
    //printf("init bad obdelniku done\n");
    startButton.setColor(game.UIcolor);


    titleLabel.setColor(game.UIcolor);
    colorLabel.setColor(game.UIcolor);
       

    fullscreenLabel.setColor(game.UIcolor);
    musicLabel.setColor(game.UIcolor);
    settingsButton.setColor(game.UIcolor);
       

    quitButton.setColor(game.UIcolor);

    printf("init menu done\n");
 
}


void menuScene::update(float deltaTime) {


    uint8_t stateOfMouse = SDL_GetMouseState(&point.x, &point.y);
    //printf("state %d\n", stateOfMouse);
    
    startButton.update(this);

    quitButton.update(this);

    settingsButton.update(this);

    //moc cringe, ale funguje xd
    float angle;
    //float theVolume;
    hsv hsvColor;
   
    if(inSettings){

        if((stateOfMouse & SDL_BUTTON_LEFT) && SDL_PointInRect(&point, &fullscreenBox1.rectangle) && !mouseWasPressed){
            if(game.fullscreen){
                game.fullscreen = false;
                SDL_SetWindowFullscreen(game.window, SDL_WINDOW_BORDERLESS);
            } else {
                game.fullscreen = true;
                SDL_SetWindowFullscreen(game.window, SDL_WINDOW_FULLSCREEN);

            }
            SDL_GetWindowSize(game.window, &game.widthOfWindow, &game.heightOfWindow);

        }

        if((stateOfMouse & SDL_BUTTON_LEFT) && SDL_PointInRect(&point, &musicLine1.rectangle)){
            musicLine2.rectangle.x = point.x;
        }

        if((stateOfMouse & SDL_BUTTON_LEFT) && SDL_PointInRect(&point, &colorLine1.rectangle)){
            colorLine2.rectangle.x = point.x;
        }
    
        if(holdingM == true){

            game.musicVolume = ((musicLine2.rectangle.x - 200.0f) / (970 - 200.0f)) * MIX_MAX_VOLUME;
            printf("volume: %f\n", game.musicVolume);
            //game.musicVolume = theVolume;
            Mix_VolumeMusic(game.musicVolume);


            if(point.x < (970 - 10)){

                musicLine2.rectangle.x = point.x;
            } else {
                musicLine2.rectangle.x = 970 - 10;
            }

            if(point.x > 200){

            } else {
                musicLine2.rectangle.x = 200;
            }
            if(!(stateOfMouse & SDL_BUTTON_LEFT))
            holdingM = false;

        } else {
            if((stateOfMouse & SDL_BUTTON_LEFT) && SDL_PointInRect(&point, &musicLine2.rectangle)){
                holdingM = true;
            }
        }

        if((stateOfMouse & SDL_BUTTON_LEFT) && SDL_PointInRect(&point, &musicLine2.rectangle)){
            musicLine2.rectangle.x = point.x;
        }


        if(holdingC == true){
            angle = ((colorLine2.rectangle.x - 200.0f) / (970 - 200.0f)) * 360 - 1;
            printf("angle: %f\n", angle);
            hsvColor.s = 1;
            hsvColor.v = 1;
            hsvColor.h = angle;
            game.UIcolor = hsv2rgb(hsvColor);

            

            printf("%f, %f, %f\n", game.UIcolor.r, game.UIcolor.g, game.UIcolor.b);
            
           
            startButton.setColor(game.UIcolor);
            titleLabel.setColor(game.UIcolor);
            colorLabel.setColor(game.UIcolor);
            fullscreenLabel.setColor(game.UIcolor);
            musicLabel.setColor(game.UIcolor);
            settingsButton.setColor(game.UIcolor);
            quitButton.setColor(game.UIcolor);


            if(point.x < (970 - 10)){

                colorLine2.rectangle.x = point.x;
                
            } else {
                colorLine2.rectangle.x = 970 - 10;
            }

            if(point.x > 200){

            } else {
                colorLine2.rectangle.x = 200;
            }
            if(!(stateOfMouse & SDL_BUTTON_LEFT))
            holdingC = false;
        } else {
            if((stateOfMouse & SDL_BUTTON_LEFT) && SDL_PointInRect(&point, &colorLine2.rectangle)){
                holdingC = true;
            }
        }
        
        
        if((stateOfMouse & SDL_BUTTON_LEFT) && SDL_PointInRect(&point, &colorLine2.rectangle)){
            colorLine2.rectangle.x = point.x;
          
        }
    }

    mouseWasPressed = stateOfMouse & SDL_BUTTON_LEFT;

}

void menuScene::drawSettings(SDL_Renderer* renderer){
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

    settingsWindow1.render(renderer, game.UIcolor);
    settingsWindow2.render(renderer, BLACK);

    colorLine1.renderOuter(renderer, WHITE);
    colorLine2.render(renderer, WHITE);

    musicLine1.renderOuter(renderer, WHITE);
    musicLine2.render(renderer, WHITE);

    fullscreenBox1.render(renderer, game.UIcolor);
    if( game.fullscreen == true)
        fullscreenBox2.render(renderer, BLACK);

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

    colorLabel.render(renderer);
    musicLabel.render(renderer);
    fullscreenLabel.render(renderer);



}


void menuScene::drawTheRest(SDL_Renderer* renderer){
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

    rightWindow1.render(renderer, game.UIcolor);
    rightWindow2.render(renderer, BLACK);

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

    titleLabel.render(renderer);
    startButton.render(renderer);
    settingsButton.render(renderer);
    quitButton.render(renderer);

}

void menuScene::render(float deltaTime) {

    SDL_RenderClear(renderer);
    
    drawTheRest(renderer);

    if(inSettings){
        drawSettings(renderer);
    }

    SDL_RenderPresent(renderer);
}